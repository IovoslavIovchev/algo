#include<algorithm>
#include<cstdint>
#include<iostream>
#include<vector>

template<typename It> constexpr void
insert_sort(It const begin, It const end) {
  for (It i = begin; i != end; ++i)
  for (It j = i ; j != begin && *j < *(j - 1); --j)
    std::swap(*j, *(j - 1));
}

int
main() {
  std::vector<int32_t> v { 64, 8, 7, 3, 0, 1 };

  insert_sort(v.begin(), v.end());

  for (auto const x : v) {
    std::cout << x << ' ';
  }

  std::cout << '\n';
}
