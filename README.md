# Algo

Implementations for various well-known algorithms in _somewhat_ modern `C++`.

Each example can be built using `make` and specifying the algorithm. For instance:
```bash
make floyd_warshall
```