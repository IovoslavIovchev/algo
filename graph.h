#pragma once

#include<cstdint>
#include<tuple>
#include<vector>
#include<unordered_map>

/**
 * The first value is the second node of the edge
 * The second value is the weight of the edge
 */
template<typename T = uint64_t>
using edge_t = std::tuple<T, uint64_t>;

/**
 * The first value is the node
 * The second value is the shortest distance to that node
 */
template<typename T = uint64_t>
using node_state_t = std::tuple<T, uint64_t>;

/**
 * The key is the node
 * The value is a vector of the adjacent nodes
 */
template<typename T = uint64_t>
using graph = std::unordered_map<T, std::vector<edge_t<T>>>;
