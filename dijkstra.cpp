#include<algorithm>
#include<limits>
#include<iostream>
#include<optional>
#include<queue>

#include "graph.h"

template<typename T> std::optional<uint64_t>
get_shortest_path(graph<T> const& g, T const start, T const destination)
{
    std::unordered_map<T, uint64_t> dist;
    std::priority_queue<node_state_t<T>, std::vector<node_state_t<T>>, std::greater<node_state_t<T>>> pq;

    for (auto const [node, _] : g) dist[node] = std::numeric_limits<T>::max();

    dist[start] = 0;
    pq.emplace(std::make_tuple(start, 0));

    while (!pq.empty())
    {
        auto const [node, shortest_dist] = pq.top();
        pq.pop();

        if (node == destination) return shortest_dist;

        if (shortest_dist > dist[node]) continue;

        for (auto const [to, weight] : g.at(node))
        {
            auto const next_shortest_dist = shortest_dist + weight;

            if (next_shortest_dist < dist[to])
            {
                pq.emplace(std::make_tuple(to, next_shortest_dist));
                dist[to] = next_shortest_dist;
            }
        }
    }

    return std::nullopt;
}

int
main()
{
#define mt std::make_tuple
    graph<uint64_t> g {
        { 0, {
            mt( 1, 1 ), mt( 3, 4 )
        }},
        { 1, {
            mt( 2, 13 ), mt( 3, 2 )
        }},
        { 2, {
            mt( 1, 1 ), mt( 3, 3 ), mt( 4, 1 )
        }},
        { 3, {
            mt( 0, 7 ), mt( 1, 7 ), mt( 4, 2 )
        }},
        { 4, {}},
    };
#undef mt

    auto const f = [&](uint64_t const start, uint64_t const dest) {
        auto const v = get_shortest_path<uint64_t>(g, start, dest);

        std::cout << "The shortest path from " << start << " to " << dest;

        if (auto l = v) {
            std::cout << " is " << *l << '\n';
        }
        else {
            std::cout << " does not exist\n";
        }
    };

    f(0, 1);
    f(0, 2);
    f(0, 3);
    f(0, 4);
    f(1, 0);
    f(2, 1);
    f(4, 1);
    f(4, 0);
}
