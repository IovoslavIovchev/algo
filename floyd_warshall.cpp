#include<limits>
#include<iostream>

#include "matrix.h"

using grid = matrix_t<int32_t>;

inline void const
calculate_distances(grid& g)
{
    for (size_t k = 0; k < g.width; ++k)
    for (size_t i = 0; i < g.width; ++i)
    for (size_t j = 0; j < g.width; ++j)
    {
        auto const cur = g.at(i, j);
        auto const alt = g.at(i, k) + g.at(k, j);

        if (cur > alt)
        {
            g.at(i, j) = alt;
        }
    }
}

int
main()
{
    auto const int_max = 1000000;

    grid g { 4, 4, std::unique_ptr<int32_t[]>(new int32_t[16] {
        0, int_max, -2, int_max,
        4, 0, 3, int_max,
        int_max, int_max, 0, 2,
        int_max, -1, int_max, 0
    })};

    calculate_distances(g);

    for (size_t i = 0; i < 16; ++i)
    {
        std::cout << g.cells[i] << '\n';
    }
}
