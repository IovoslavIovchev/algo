CC=g++
FLGS=-Wall -pedantic-errors -g
STD=c++2a

DEPS=matrix.h

%: %.cpp $(DEPS)
	$(CC) $(FLGS) -std=$(STD) $@.cpp -o $@
