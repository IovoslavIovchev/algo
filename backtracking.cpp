#include<iostream>
#include<stack>
#include<functional>

#include "matrix.h"

using maze = matrix_t<uint8_t>;
using coordinates_t = std::pair<size_t, size_t>;

enum cell_state
{
    unvisited = 0,
    visited = 1,
    wall = 7
};

enum direction
{
    N = (1 << 1),
    E = (1 << 2),
    S = (1 << 3),
    W = (1 << 4)
};

// returns the available neighbours as an 8-bit integer,
// which uses the first 4 bits to represent the directions
inline uint8_t const
get_available_neighbours(coordinates_t const c, maze const& m)
{
    auto const [x, y] = c;

    auto const is_available = [&](uint32_t x, uint32_t y) -> bool
    {
        bool const is_in_range = x < m.width && y < m.height;

        // only check for availability if the coordinates are within the bounds of the maze
        return is_in_range && m.at(x, y) == cell_state::unvisited;
    };

    uint8_t const N = is_available(x, y - 1) ? direction::N : 0;
    uint8_t const E = is_available(x + 1, y) ? direction::E : 0;
    uint8_t const S = is_available(x, y + 1) ? direction::S : 0;
    uint8_t const W = is_available(x - 1, y) ? direction::W : 0;

    return N | E | S | W;
}

bool const
traverse(maze const& m,
         coordinates_t const start,
         coordinates_t const destination,
         std::function<void(coordinates_t const)> f)
{
    std::stack<coordinates_t> stack;
    stack.push(start);

    while (!stack.empty())
    {
        auto const top = stack.top();
        auto const [x, y] = top;

        // mark the current cell as already visited
        m.at(x, y) = cell_state::visited;

        f(top);

        if (top == destination)
        {
            // in case we reached our destination there is nothing left for us to do
            return true;
        }

        uint8_t const available_neighbours = get_available_neighbours(top, m);

        // if there are no available neighbours, we need to backtrack
        if (available_neighbours == 0)
        {
            stack.pop();
            continue;
        }

        if ((available_neighbours & direction::N) > 0) stack.push(std::make_pair(x, y - 1));
        if ((available_neighbours & direction::E) > 0) stack.push(std::make_pair(x + 1, y));
        if ((available_neighbours & direction::S) > 0) stack.push(std::make_pair(x, y + 1));
        if ((available_neighbours & direction::W) > 0) stack.push(std::make_pair(x - 1, y));
    }

    // indicate that we were not able to reach our destination
    return false;
}

int
main()
{
    // a positive scenario
    maze m1 { 5, 5, std::unique_ptr<uint8_t[]>(new uint8_t[25] {
        0, 7, 0, 0, 0,
        0, 0, 0, 7, 0,
        0, 7, 7, 0, 0,
        0, 7, 7, 0, 7,
        0, 7, 0, 0, 0
            })};

    bool const a_1 = traverse(m1, std::make_pair(0, 0), std::make_pair(4, 4), [](coordinates_t c) {
        std::cout << "x: " << c.first << " y: " << c.second << '\n';
            });

    std::cout << "Attempt 1 " << (a_1 ? "successful" : "failed") << '\n';

    // a negative scenario
    maze m2 { 5, 5, std::unique_ptr<uint8_t[]>(new uint8_t[25] {
        0, 7, 0, 0, 0,
        0, 0, 0, 7, 0,
        0, 7, 0, 7, 0,
        0, 7, 7, 0, 7,
        0, 7, 0, 0, 0
            })};

    bool const a_2 = traverse(m2, std::make_pair(2, 2), std::make_pair(4, 4), [](coordinates_t c) {
        std::cout << "x: " << c.first << " y: " << c.second << '\n';
            });

    std::cout << "Attempt 2 " << (a_2 ? "successful" : "failed") << '\n';
}
