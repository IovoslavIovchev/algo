#include<cstdint>
#include<iostream>
#include<vector>

template<typename T, typename It, typename Cmp> inline constexpr bool
contains(It begin, It end, T const x, Cmp const cmp) {
  while (begin < end) {
    auto const mid = begin + (end - begin) / 2;

    if (cmp(*mid, x)) {
      begin = mid + 1;
    }
    else {
      end = mid;
    }
  }

  return *begin == x;
}

template<typename T, typename It> inline constexpr bool
contains(It begin, It end, T const x) {
  return contains(begin, end, x, [](T const& l, T const& r) { return l < r; });
}

int
main() {
  std::vector<int64_t> v { 1, 1, 2, 3, 3, 3, 3, 4, 4, 4, 5, 5, 6, 13, 82, 99, 3004 };

  auto const print_contains = [&](int64_t x) {
    std::cout
      << std::boolalpha
      << "v contains " << x << "? "
      << contains(v.begin(), v.end(), x)
      << '\n';
  };

  print_contains(3);    // true
  print_contains(9);    // false
  print_contains(98);   // false
  print_contains(3004); // true
}
