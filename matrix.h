#pragma once

#include<memory>

template<typename T>
struct matrix_t
{
    size_t const height;
    size_t const width;
    std::unique_ptr<T[]> const cells;

    T& at(size_t x, size_t y) const
    {
        return cells[x + y * width];
    }
};
